# colour-of-things

Colour of things is a project which was used to play with CSS grid and pretty colours.

It was inspired by [Werner's Nomenclature of Colours](https://books.google.co.uk/books/about/Werner_s_Nomenclature_of_Colours.html?id=duZFDwAAQBAJ&printsec=frontcover&source=kp_read_button&redir_esc=y#v=onepage&q&f=false)

[Demo](https://colour-of-things.firebaseapp.com/)

There is no content on each colour as of yet, but the idea is that each colour has different animals/minerals etc that match that colour with information about it.