module.exports = {
  build: {
    postcss: [
      require('postcss-cssnext')({
        features: {
          customProperties: true
        }
      })
    ]
  }
}